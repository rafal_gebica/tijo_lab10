package pl.edu.pwsztar.service.moveServiceImpl;

import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.enums.FigureType;
import pl.edu.pwsztar.service.MoveService;

@Service
public class MoveServiceImpl implements MoveService {
    @Override
    public Boolean isMoveCorrect(FigureMoveDto figureMoveDto) {
        if(FigureType.BISHOP == figureMoveDto.getType()) {
            return checkMove(figureMoveDto);
        } else {
            return false;
        }
    }
    private Boolean checkMove(FigureMoveDto figureMoveDto) {
        String[] startPoint = figureMoveDto.getStart().split("_");
        String[] destinationPoint = figureMoveDto.getDestination().split("_");

        int startX = startPoint[0].charAt(0);
        int startY = Integer.parseInt(startPoint[1]);
        int destinationX = destinationPoint[0].charAt(0);
        int destinationY = Integer.parseInt(destinationPoint[1]);

        return Math.abs(startX - destinationX) == Math.abs(startY - destinationY);
    }
}
