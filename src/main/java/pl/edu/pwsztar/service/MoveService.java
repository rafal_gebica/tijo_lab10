package pl.edu.pwsztar.service;


import pl.edu.pwsztar.domain.dto.FigureMoveDto;

@FunctionalInterface
public interface MoveService {
    Boolean isMoveCorrect(FigureMoveDto figureMoveDto);
}

